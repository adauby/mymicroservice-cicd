let express = require('express');
let app = express();

app.get('/', function (req, res) {
    res.json({ 
        products: [{
            name: "laptop", 
            price: 500
        }, {
            name: "monitor",
            price: 100
        }, {
            name: "keyboard",
            price: 30
        }]
    });
    res.end();
});

//listen on port 3001
app.listen(3001, function () {
  console.log("application listening on port 3001!");
});

